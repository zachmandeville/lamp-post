# Lamp Post: The Place to Put Your Party Posters!

This is an extension of the work done on [wheat-paste](https://gitlab.com/zachmandeville/wheat-paste).  Wheat pastes create individual parties, but a person may want to see ALL the parties they are supporting/hosting.  In other words, you wanna go to the lamp post that has all the good flyers, and see what's coming up.

Hence, Lamp-Post.  This dat site lets you put in the url of a wheat-paste, and it'll pull in all the details and display them on your individual Post.  If you'd like, you could then share this lamp-post with others, so they can see yr good party tastes.

# Caveats

THIS IS EARLY AS HELL.  The basic proof of concept works, but yr seeing a pre-alpha of a thang.

# Thanks

This app was inspired by a Paul Frazee livestream, and the webring he built in vanilla javascript.



