const MS_IN_A_DAY = 24*60*60*1000
import {$, clone} from './util.js'

function addPartyToPage (partyData) {
  var partyItemTemplate = $('#party-item-template')
  var el = clone(partyItemTemplate)
  console.log({el})
  $(el, '.date').textContent = daysUntil(new Date(partyData.startDate))
  $(el, '.time').textContent = partyData.startTime
  $(el, '.title').textContent = partyData.title
  $(el, '.location').textContent = partyData.location
  $('#party-list').appendChild(el)
}

var today = Date.now()
function daysUntil (date) {
  var diffDays = Math.floor(Math.abs(date.getTime() - today) / MS_IN_A_DAY)
  if (diffDays === 0) return 'today!'
  if (diffDays === 1) return 'tomorrow'
  if (diffDays > 14) return date.toLocaleDateString()
  return `in ${diffDays} days.`
}

async function pullPartyDeets (dat) {
  var archive = new DatArchive(dat.url)
  var partyJsonRaw = await archive.readFile('/party.json', 'utf-8')
  var party = JSON.parse(partyJsonRaw)
  addPartyToPage(party)
}

async function onPageLoad () {
  var archive = new DatArchive(window.location)
  var postersJsonRaw = await archive.readFile('/posters.json', 'utf-8')
  var posters = JSON.parse(postersJsonRaw)
  posters.forEach(pullPartyDeets)
}

async function fun (e) {
  e.preventDefault()
  var archive = new DatArchive(window.location)
  var form = e.currentTarget
  var body = getFormData(form)
  var partyRaw = await archive.readFile('/posters.json')
  var parties = JSON.parse(partyRaw)
  var string = JSON.stringify([...parties, body], null, 2)
  archive.writeFile('/posters.json', string)
    .then(console.log('success'))
}


function getFormData (form) {
  var data = {}
  var formData = new FormData(form)
  for (var pair of formData.entries()) {
    data[pair[0]] = pair[1]
  }
  return data
}

onPageLoad()

$('#now-party').onsubmit = fun
